import java.util.*;
class Main{
	void area(int value){
		 System.out.println("Area of a circle is "+ (3.14*value*value)+"cm^2");
	}
	void circumference(int value){
		 System.out.print("Circumference of a circle is "+(2*3.14*value)+"cm");
	}
}
class Circle{
    public static void main(String args[]){

	   Scanner input=new Scanner(System.in);
	   Main obj=new Main();

	   System.out.print("Enter the radius(cm):");
	   int data=input.nextInt();
	   obj.area(data);
	   obj.circumference(data);
}
}

import java.util.*;
class DecPattern{
	public static void main(String args[]){
		Scanner input=new Scanner(System.in);
		System.out.print("Enter number of rows:");
		int row=input.nextInt();

		for(int i=row;i>=1;i--){
			for(int j=row;j>=i;j--){
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}
}
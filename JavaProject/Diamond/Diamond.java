import java.util.*;

class Main{
	void star(int row){
		for(int i=1;i<=row;i++){
        	for(int j=row-i;j>=1;j--){
        		System.out.print(" ");
        	}
        	for(int j=1;j<=i;j++){
                System.out.print("* ");
        		}
        	System.out.println();
        }
	}
	void reverse(int row){
		for(int i=row-1;i>=1;i--){
        	for(int j=row-i;j>=1;j--){
        		System.out.print(" ");
        	}
        	for(int j=1;j<=i;j++){
                System.out.print("* ");
        	}
        	System.out.println();
        }
	}
}
class Diamond{
	public static void main(String args[]){
		Main obj=new Main();
        Scanner input=new Scanner(System.in);
        System.out.print("Enter number of rows:");
        int row=input.nextInt();

        obj.star(row);
        obj.reverse(row);
	}
}
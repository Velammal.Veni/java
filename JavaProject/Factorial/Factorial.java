import java.util.*;
class Main{
	int sum=1;

	void fact(int value){
		for(int i=1;i<=value;i++){
		sum=sum*i;       
		}
		System.out.println("The Factorial of "+ value + " is "+ sum);
	}
}
class Factorial{
	public static void main(String args[]){
		Main obj=new Main();
		Scanner input=new Scanner(System.in);
		System.out.print("Enter a number:");
		int data=input.nextInt();
		obj.fact(data);
	}
}
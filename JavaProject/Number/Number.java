import java.util.*;
class Number{
	public static void main(String args[]){ 
		Scanner input=new Scanner(System.in);
		System.out.print("Enter number of row:");
		int row=input.nextInt();
		
		for(int i=1;i<=row;i++){
			for(int j=i;j>=1;j--){
				System.out.print(j+" " );
			}
			    System.out.println();
		}
	}
}
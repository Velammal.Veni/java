import java.util.*;
class BasicCriteria{
	public static boolean CheckForAgeCriteria(int age){
		if(age>=18 && age<=20){
			return true;
		}
		else{
			return false;
		}
	}
	public static boolean CheckForDegreeCriteria(String degree){
		if(degree.equals("Higher Secondary") || degree.equals("Diplomo")){
			return true;
		}
		else{
			return false;
		}
	}
}
class Technology extends BasicCriteria{
	public static boolean CheckForMathsAbility(String grade){
		if(grade.equals("Good") || grade.equals("Very Good")){
			return true;
		}
		else{
			return false;
		}
	}
	public static boolean CheckForProgAbility(String grade2){
		if(grade2.equals("Good") || grade2.equals("Very Good")){
			return true;
		}
		else{
			return false;
		}
	}
}
// class Design extends BasicCriteria{
// 	public static boolean CheckForToolKnowledge(String grade){
// 		if(grade.equals("Good") || grade.equals("Very Good")){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}
// 	public static boolean CheckForDrawingAbility(String grade2){
// 		if(grade2.equals("Good") || grade2.equals("Very Good")){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}
// }
// class Biz extends BasicCriteria{
// 	public static boolean CheckForEnglishKnowledge(String grade){
// 		if(grade.equals("Good") || grade.equals("Very Good")){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}
// 	public static boolean CheckForStatisticsAbility(String grade2){
// 		if(grade2.equals("Good") || grade2.equals("Very Good")){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}
// }
class Schools{
	public static void main(String args[]){
		Technology obj1=new Technology();
		if(
		obj1.CheckForAgeCriteria(19) &&
		obj1.CheckForDegreeCriteria("Higher Secondary") &&
		obj1.CheckForMathsAbility("Very Good") &&
		obj1.CheckForProgAbility("Good")){
			System.out.println("You are elgibile for school of Technology");
		}
		else{
			System.out.println("You are not elgibile for school of Technology");
		}

		// Design obj1=new Design();
		// if(
		// obj1.CheckForAgeCriteria(19) &&
		// obj1.CheckForDegreeCriteria("Higher Secondary") &&
		// obj1.CheckForToolKnowledge("Very Good") &&
		// obj1.CheckForDrawingAbility("Good")){
		// 	System.out.println("You are elgibile for school of Design");
		// }
		// else{
		// 	System.out.println("You are not elgibile for school of Design");
		// }

		// Biz obj1=new Biz();
		// if(
		// obj1.CheckForAgeCriteria(19) &&
		// obj1.CheckForDegreeCriteria("Higher Secondary") &&
		// obj1.CheckForEnglishKnowledge("Very Good") &&
		// obj1.CheckForStatisticsAbility("Good")){
		// 	System.out.println("You are elgibile for school of Business");
		// }
		// else{
		// 	System.out.println("You are not elgibile for school of Business");
		// }
	}
}
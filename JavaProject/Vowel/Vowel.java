import java.util.*;
class Vowel{
	String vowel(char letter){
		if(letter=='a'||letter=='e'||letter=='i'||letter=='o'||letter=='u'||letter=='A'|letter=='E'|letter=='I'|letter=='O'|letter=='U'){
			return "True,the given data has vowel.";
		}
		else{
			return "False,the given data don't have vowel.";
		}
	}
	public static void main(String args[]){
		Scanner input=new Scanner(System.in);
		Vowel obj=new Vowel();

		System.out.print("Enter a letter:");
        char ans=input.next().charAt(0);
        System.out.println(obj.vowel(ans));
        System.out.print("Enter a string:");
        input.nextLine();
        String sen=input.nextLine();
        int num=0;
        for(int i=0;i<sen.length();i++){
			if(sen.charAt(i)=='a'||sen.charAt(i)=='e'||sen.charAt(i)=='i'||sen.charAt(i)=='o'||sen.charAt(i)=='u'||sen.charAt(i)=='A'||sen.charAt(i)=='E'||sen.charAt(i)=='I'||sen.charAt(i)=='O'||sen.charAt(i)=='U'){
				num++;
			}
		}
		System.out.println("The number of vowels in the string is "+num); 
	}
}
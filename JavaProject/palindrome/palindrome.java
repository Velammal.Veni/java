import java.util.Scanner;
class palindrome{
  public String check(int number){
     String answer="False,it's not a palindrome";
     int rem,sum=0;
     int temp=number;
     while(number>0){
        rem=number%10;
        sum=(sum*10)+rem;
        number/=10;
     }
     if(temp==sum){
       answer="True,it's a palindrome";
     }
     return answer;
  }
  public static void main(String[] args){
   palindrome obj=new palindrome();
   Scanner scan=new Scanner(System.in);
   System.out.print("please enter the number:");
   int input=scan.nextInt();
   String answer=obj.check(input);
   System.out.println(answer);
  }
}
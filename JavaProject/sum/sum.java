import java.util.Scanner;
public class sum{
	public static void main(String args[]){
    Scanner input=new Scanner(System.in);

    System.out.print("Enter a number between 1 and 1000:");
    int data= input.nextInt();

    int digit;
    int sum=0;

    if(data>1 && data<1000){
    	while(data>0){
    		digit=data%10;
    		sum  =sum+digit;
    		data =data/10;
    	}
    	System.out.println("Sum of the digits in given number is"+" "+sum);
    }
    else{
    	System.out.println("Please enter a number between 1 and 1000.");
    }

	}
}